<?php
// GetPublicEvent.php
// Searches form all events with public value 'public' for home calendar


header("Content-Type: application/json"); 

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'Calendar', 'Calendar', 'Calendar');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
	));
	exit;
}

$stmt = $mysqli->prepare("SELECT COUNT(*), user, time, title, description FROM events WHERE public=? and day=? and month=? and year=?");


// Bind the parameter
$stmt->bind_param('ssss', $public, $day, $month, $year);
$public = 'public';
$day = $_POST['day'];
$month= $_POST['month'];
$year= $_POST['year'];
$stmt->execute();


$result = $stmt->get_result();

 
while($row = $result->fetch_assoc()){
	$count= htmlentities($row["COUNT(*)"]);
	$user = htmlentities($row["user"]);
	$time = htmlentities($row["time"]);
	$title = htmlentities($row["title"]);
	$description = htmlentities($row["description"]);	
}

if($user== null){
	echo json_encode(array(
		"success" => false,
	));
	exit;
}

//publish as json
echo json_encode(array(
		"success" => true,
		"count" => $count,
		"user" => $user,
		"time" => $time,
		"title" => $title,
		"day" => $day,
		"month" => $month,
		"year" => $year,
		"description" => $description,
		"index" => $_POST['index']
		
	));
exit;

?>