//After event select gets data for edit interface
function editEvent(day, month, year, title) {
    // Make a URL-encoded string for passing POST data:
	var dataString = "day=" + encodeURIComponent(day) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year);
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CalendarGetEvent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData	
        
        displayEdit(day, month, year);
		
        }else{
            
        alert("not logged in");
            
		}
	}, false); // Bind the callback to the load event
    
	xmlHttp.send(dataString); // Send the data
}

//Display edit interface with event values
function displayEdit(day, month, year){
    
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    var x = document.getElementById("footer");
    
    x.innerHTML= "<h2> Edit Event </h2>"
    + "<label>Title: <input type=\"text\" id= \"newTitle\" value=\"" + jsonData.title + "\"></label>"
    + "<br>"
    + "<br>"
    + "<label>Tag: <input type=\"text\" id= \"newTags\" value=\"" + jsonData.tags + "\"></label>"
    + "<br>"
    + "<br>"
    + "<label>Privacy: " 
    + "<select id=\"newPublic\">"
    + "<option value=\"private\">Private</option>"
    + "<option value=\"public\">Public</option>"
    + "</select>"
    + "</label>"
    + "<br>"
    + "<br>"
    + "<label>Time: <input type=\"text\" id= \"newTime\" value=\"" + jsonData.time + "\"></label>"
    + "<br>"
    + "<br>"
    + "<label>Date: <input type=\"text\" id= \"newDay\" value=\"" + day + "\"></label>"
    + "<input type=\"text\" id= \"newMonth\" value=\"" + month + "\">"
    + "<input type=\"text\" id= \"newYear\" value=\"" + year + "\">"
    + "<br>"
    + "<br>"
    + "<textarea id= newDescription rows=\"4\" cols=\"50\" >" + jsonData.description + "</textarea>"
    + "<br>"
    + "<br>"
    + "<input id=\"editEventBtn\" type=\"button\" value= \"Edit\" onClick= \"deleteAdd("
	+ day + "," + month + "," + year + ", \'" + jsonData.title + "\')\">"
    + " <input id=\"deleteEventBtn\" type=\"button\" value= \"Delete\"  onClick= \"justDelete("
	+ day + "," + month + "," + year + ", \'" + jsonData.title + "\')\">"
    + "<br>"
    + "<br>"
    + "<label>Share: <input type=\"text\" id= \"shareUser\" value=\"Username\"></label>"
    + " <input id=\"shareEventBtn\" type=\"button\" value= \"Share\"  onClick= \"shareInit()\">";
    
}

//run functions for delete event
function justDelete(day, month, year, title){
    deleteEvent(day, month, year, title);
    createCalendar();
    updateCalendar();
    clearFooter();

}

//deletes and adds new event for edit
function deleteAdd(day, month, year, title){
    deleteEvent(day, month, year, title);
    createAlterEvent();
    
}

//start share
function shareInit(){
    createShareEvent();
    
}


function clearFooter() {
    var x = document.getElementById("footer");
    x.innerHTML= "" 
}

//sends d/m/y to delete event
function deleteEvent(day, month, year, title){
    
    var dataString = "day=" + encodeURIComponent(day) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year)
    + "&title=" + encodeURIComponent(title);
 
    
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CalendarDeleteEvent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
            
		}else{
			
			alert("Failed to delete");
            alert(jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
    
}

//adds event based on edit input function
function createAlterEvent() {

    var cday = document.getElementById("newDay").value; 
	var cmonth = document.getElementById("newMonth").value;
    var cyear = document.getElementById("newYear").value;
    var ctitle = document.getElementById("newTitle").value;
    var cdescription = document.getElementById("newDescription").value;
    var cpublic = document.getElementById("newPublic").value;
    var ctags = document.getElementById("newTags").value;
    
    var ctime = document.getElementById("newTime").value;

    var dataString = "day=" + encodeURIComponent(cday) + "&month=" + encodeURIComponent(cmonth) + "&year=" + encodeURIComponent(cyear)
    + "&title=" + encodeURIComponent(ctitle) + "&description=" + encodeURIComponent(cdescription)
    + "&time=" + encodeURIComponent(ctime) + "&tags=" + encodeURIComponent(ctags) + "&public=" + encodeURIComponent(cpublic);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CalendarCreateEvent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			
            alert("event changed!");
            createCalendar();
            updateCalendar();
            clearFooter();
            
		}else{
			
			alert("Failed");
            alert(jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
 
}

//takes in user and adds event to their calendar
function createShareEvent() {
    
    var cday = document.getElementById("newDay").value; 
	var cmonth = document.getElementById("newMonth").value;
    var cyear = document.getElementById("newYear").value;
    var ctitle = document.getElementById("newTitle").value;
    var cpublic = "private";
    var ctags = "Share";
    
    var ctime = document.getElementById("newTime").value;
    var cuser = document.getElementById("shareUser").value;

    var dataString = "day=" + encodeURIComponent(cday) + "&month=" + encodeURIComponent(cmonth) + "&year=" + encodeURIComponent(cyear)
    + "&title=" + encodeURIComponent(ctitle)
    + "&time=" + encodeURIComponent(ctime) + "&tags=" + encodeURIComponent(ctags) + "&public=" + encodeURIComponent(cpublic)
    + "&username=" + encodeURIComponent(cuser);
    
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CreateShareEvent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData

            alert("event shared!");
            clearFooter();
            
		}else{
			alert("Failed");
            alert(jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
 
}