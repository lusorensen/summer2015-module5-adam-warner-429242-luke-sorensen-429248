//runs during every iteration of calendar day
//gets event info
function getEvent(day, month, year, index) {

	var dataString = "day=" + encodeURIComponent(day) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year) + "&index=" + encodeURIComponent(index);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CalendarGetEvent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData	
        
        addEvent(day, month, year);
		
        }else{
            
		}
	}, false);
    
	xmlHttp.send(dataString);
}

//add json from get event to calendar
function addEvent(day, month, year){
    var jsonData = JSON.parse(event.target.responseText); 
    var cell = document.getElementById(jsonData.index);
	cell.innerHTML+= "<br>";
	cell.innerHTML += "<input type=button value=\"" + jsonData.title + "\" onClick= \"editEvent("
	+ day + "," + month + "," + year + ", \'" + jsonData.title + "\')\">";
	cell.innerHTML += "<br>"+ "<label> Tags: " + jsonData.tags + "</label>";
	cell.innerHTML += "<br>"+ "<label> Description: " + jsonData.description + "</label>";
}

//takes in necesary data
//creates event
function createEvent() {

    var cday = document.getElementById("eventDay").value; 
	var cmonth = document.getElementById("eventMonth").value;
    var cyear = document.getElementById("eventYear").value;
    var ctitle = document.getElementById("eventTitle").value;
    var cdescription = document.getElementById("eventDescription").value;
	var cpublic = document.getElementById("eventPublic").value;
    
    var chour = document.getElementById("eventHour").value;
    var cminute = document.getElementById("eventMinute").value;
    
    var ctime= chour + cminute;
    
    var ctags = document.getElementById("eventTags").value;
    
    var dataString = "day=" + encodeURIComponent(cday) + "&month=" + encodeURIComponent(cmonth) + "&year=" + encodeURIComponent(cyear)
    + "&title=" + encodeURIComponent(ctitle) + "&description=" + encodeURIComponent(cdescription)
    + "&time=" + encodeURIComponent(ctime) + "&tags=" + encodeURIComponent(ctags) + "&public=" + encodeURIComponent(cpublic);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CalendarCreateEvent.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			
			alert("Event Added!");
            createCalendar();
            updateCalendar();
	
		}else{
            alert(jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
 
}