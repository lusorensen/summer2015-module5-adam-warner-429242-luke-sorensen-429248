//changes header to username and log out button if logged in
function headerLoggedIn() {
	
	var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
	var x = document.getElementById("header");
	x.innerHTML = "<h1>" + jsonData.user + "\'s Calendar </h1>"	
	+ "<input id=\"logOutBtn\" type=\"button\" value= \"LogOut\" onClick=\"logOut()\">"
    + "<br><br>";
	
	var y = document.getElementById("tagSearch");
	y.innerHTML = "<h2>Tag Search</h2>"
    + "<label>Enter Search: <input type=\"text\" id= \"tagSearchFrm\" value=\"Search\"></label>"
	+ "  <input id=\"tagSearchBtn\" type=\"button\" value= \"SearchTag\" onClick=\"searchTag()\">";
    
}

//changes header to login form if logged out
function headerLoggedOut() {

	var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
	var x = document.getElementById("header");
	
	x.innerHTML = "<h1> Calendar </h1>"
    + "<label>Enter Username: <input type=\"text\" id= \"username\" name=\"username\"></label>"
	+ "<br>"
	+ "<br>"
	+ "<label>Enter Password: <input type=\"password\" id= \"password\" name=\"password\"></label>"
	+ "<br>"
	+ "<br>"
	+ "<input id=\"loginBtn\" type=\"button\" value= \"Login\" onClick=\"logIn()\">"
	+ " <input id=\"registerBtn\" type=\"button\" value= \"Register\" onClick=\"register()\">"
    + "<br>"
	+ "<br>";
	
	var y = document.getElementById("tagSearch");
	y.innerHTML = "";
	
	var z = document.getElementById("footer");
	z.innerHTML = "";
	
}


