<?php
// CalendarGetEvent.php
// Takes in d/m/y and user and gets event on given day


header("Content-Type: application/json");

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'Calendar', 'Calendar', 'Calendar');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
	));
	exit;
}

$stmt = $mysqli->prepare("SELECT COUNT(*), user, time, title, tags, description FROM events WHERE user=? and day=? and month=? and year=?");


// Bind the parameter
$stmt->bind_param('ssss', $username, $day, $month, $year);
$username = $_SESSION['username'];
$day = $_POST['day'];
$month= $_POST['month'];
$year= $_POST['year'];
$stmt->execute();


$result = $stmt->get_result();

 
while($row = $result->fetch_assoc()){
	$count= htmlentities($row["COUNT(*)"]);
	$user = htmlentities($row["user"]);
	$time = htmlentities($row["time"]);
	$title = htmlentities($row["title"]);
	$tags = htmlentities($row["tags"]);
	$description = htmlentities($row["description"]);	
}

if($user== null){
	echo json_encode(array(
		"success" => false,
	));
	exit;
}

//create json for calendar data
echo json_encode(array(
		"success" => true,
		"count" => $count,
		"user" => $username,
		"time" => $time,
		"tags" => $tags,
		"title" => $title,
		"day" => $day,
		"month" => $month,
		"year" => $year,
		"description" => $description,
		"index" => $_POST['index']
		
	));
exit;

?>