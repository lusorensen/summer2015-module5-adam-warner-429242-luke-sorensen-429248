<!DOCTYPE html>
<html>
<head>
<title>Calendar Main</title>
<style>
	
body{background-color: #FFEBFF; 
	margin: 1em}
	
table{background-color: #F5FFF5;
	
}

h1{color:#002EB8;
	
}

</style>

<script type="text/javascript" src="LogInOut.js"></script>
<script type="text/javascript" src="UpdateHeader.js"></script>
<script type="text/javascript" src="GetDates.js"></script>
<script type="text/javascript" src="EditEvent.js"></script>
<script type="text/javascript" src="ManageEvents.js"></script>
<script type="text/javascript" src="RenderCalendar.js"></script>
<script type="text/javascript" src="TagEvents.js"></script>
<script type="text/javascript" src="PublicEvents.js"></script>
<script type="text/javascript" src="NextLast.js"></script>

<script>
	
document.addEventListener("DOMContentLoaded", createPublicCalendar, false);
document.addEventListener("DOMContentLoaded", updateCalendarPublic, false);
document.addEventListener("DOMContentLoaded", function(){

    document.getElementById("addEventBtn").addEventListener("click", createEvent, false);
    document.getElementById("loginBtn").addEventListener("click", logIn, false);
    document.getElementById("registerBtn").addEventListener("click", register, false);
    document.getElementById("logOutBtn").addEventListener("click", logOut, false);

}, false);


</script>
</head> 
<body>
	
<!--div for header, changes with login, logout-->
<div id="header">
<h1>Calendar</h1>
<label>Enter Username: <input type="text" id= "username" name="username"></label>
<br>
<br>
<label>Enter Password: <input type="password" id= "password" name="password"></label>
<br>
<br>
<input id="loginBtn" type="button" value= "Login">
<input id="registerBtn" type="button" value= "Register">
<br>
<br>
</div>

<!--div for calendar-->
<div id="Calendar">
</div>

<!--add event forms, only works when user logged in-->
<h2>Add Event</h2>

<label>Title: <input type="text" id= "eventTitle" name="Title"></label>
<br>
<br>
<label>Tag: <input type="text" id= "eventTags" name="Tags" placeholder="Optional"></label>
<br>
<br>
<label>Privacy: 
<select id="eventPublic">
	<option value="private">Private</option>
	<option value="public">Public</option>
</select>
</label>

<br>
<br>

<label>Date: 
<select id="eventMonth">
	<option value="1">January</option>
	<option value="2">February</option>
	<option value="3">March</option>
	<option value="4">April</option>
	<option value="5">May</option>
	<option value="6">June</option>
	<option value="7">July</option>
	<option value="8">August</option>
	<option value="9">September</option>
	<option value="10">October</option>
	<option value="11">November</option>
	<option value="12">December</option>
</select>
</label>

<select id="eventDay">
    <option value="1">01</option>
    <option value="2">02</option>
    <option value="3">03</option>
    <option value="4">04</option>
    <option value="5">05</option>
    <option value="6">06</option>
    <option value="7">07</option>
    <option value="8">08</option>
    <option value="9">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
</select>

<input type="text" id= "eventYear" placeholder="Year XXXX">
<br>
<br>

<label> Time:
<select id="eventHour">
    <option value="00">00</option>
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
</select>
</label>

<label>:
<select id="eventMinute">
    <option value="00">00</option>
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
    <option value="32">32</option>
    <option value="33">33</option>
    <option value="34">34</option>
    <option value="35">35</option>
    <option value="36">36</option>
    <option value="37">37</option>
    <option value="38">38</option>
    <option value="39">39</option>
    <option value="40">40</option>
    <option value="41">41</option>
    <option value="42">42</option>
    <option value="43">43</option>
    <option value="44">44</option>
    <option value="45">45</option>
    <option value="46">46</option>
    <option value="47">47</option>
    <option value="48">48</option>
    <option value="49">49</option>
    <option value="50">50</option>
    <option value="51">51</option>
    <option value="52">52</option>
    <option value="53">53</option>
    <option value="54">54</option>
    <option value="55">55</option>
    <option value="56">56</option>
    <option value="57">57</option>
    <option value="58">58</option>
    <option value="59">59</option>

</select>
</label>

<br>
<br>

<textarea id= eventDescription rows="4" cols="50" name="Comment" placeholder= "Description(Optional)"></textarea>
<br>
<br>
<input id="addEventBtn" type="button" value= "AddEvent">

<!--div for search by tags, appears when user logged in-->
<div id="tagSearch">

</div>

<!--div for edit event interface-->
<div id="footer">
</div>
</body>
</html>