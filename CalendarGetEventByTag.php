<?php
// GetEventByTag.php
// Searches event by tag, day and user, returns event

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'Calendar', 'Calendar', 'Calendar');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
	));
	exit;
}

$stmt = $mysqli->prepare("SELECT COUNT(*), user, time, title, description FROM events WHERE user= ? and tags=? and day=? and month=? and year=?");


// Bind the parameter
$stmt->bind_param('sssss', $user, $tag, $day, $month, $year);
$user= $_SESSION['username'];
$tag = $_POST['tag'];
$day = $_POST['day'];
$month= $_POST['month'];
$year= $_POST['year'];
$stmt->execute();


$result = $stmt->get_result();

 
while($row = $result->fetch_assoc()){
	$count= htmlentities($row["COUNT(*)"]);
	$user = htmlentities($row["user"]);
	$time = htmlentities($row["time"]);
	$title = htmlentities($row["title"]);
	$description = htmlentities($row["description"]);	
}

if($user== null){
	echo json_encode(array(
		"success" => false,
	));
	exit;
}
echo json_encode(array(
		"success" => true,
		"count" => $count,
		"user" => $username,
		"time" => $time,
		"title" => $title,
		"day" => $day,
		"month" => $month,
		"year" => $year,
		"description" => $description,
		"index" => $_POST['index']
		
	));
exit;

?>