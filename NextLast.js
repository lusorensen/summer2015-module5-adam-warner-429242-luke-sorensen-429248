var currentMonth = new Month(2015, 6); 

function nextMonth(event){
	currentMonth = currentMonth.nextMonth(); // Previous month would be currentMonth.prevMonth() 
	updateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
}

function prevMonth(event){
	currentMonth = currentMonth.prevMonth(); // Previous month would be currentMonth.prevMonth()
	updateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
}

function nextPublicMonth(event){
	currentMonth = currentMonth.nextMonth(); // Previous month would be currentMonth.prevMonth()
	updateCalendarPublic(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
}

function prevPublicMonth(event){
	currentMonth = currentMonth.prevMonth(); // Previous month would be currentMonth.prevMonth()
	updateCalendarPublic(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
}

function nextTagMonth(){
	var tag= document.getElementById("tagSearchFrm").value;
	currentMonth = currentMonth.nextMonth(); // Previous month would be currentMonth.prevMonth()
	updateCalendarTag(tag); // Whenever the month is updated, we'll need to re-render the calendar in HTML
}

function prevTagMonth(){
	var tag= document.getElementById("tagSearchFrm").value;
	currentMonth = currentMonth.prevMonth(); // Previous month would be currentMonth.prevMonth()
	updateCalendarTag(tag); // Whenever the month is updated, we'll need to re-render the calendar in HTML
}