<?php
//CalendarShareEvent.php
//Takes in post user name and adds event to table
//Description will show user who shared event

header("Content-Type: application/json"); 
ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'Calendar', 'Calendar', 'Calendar');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Can't connect"
	));
	exit;
}

if(empty($_SESSION["username"])){
	echo json_encode(array(
		"success" => false,
		"message" => "Not logged in"
	));
	exit;
}

$user= htmlentities($_POST['username']);
$day = htmlentities($_POST['day']);
$month = htmlentities($_POST['month']);
$year = htmlentities($_POST['year']);
$title= htmlentities($_POST['title']);
$time = htmlentities($_POST['time']);
$description= htmlentities($_SESSION['username']);
$tags = htmlentities($_POST['tags']);
$public = htmlentities($_POST['public']);


$stmt1 = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");

// Bind the parameter
$stmt1->bind_param('s', $user);
$stmt1->execute();

$result = $stmt1->get_result();

while($row = $result->fetch_assoc()){
	$count= htmlentities($row["COUNT(*)"]);	
}

if($count==1){

//upload story with title, link, poster, date, commentary into stories table
$stmt = $mysqli->prepare("insert into events (user, day, month, year, time, title, description, tags, public) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
if(!$stmt){
    echo json_encode(array(
		"success" => false,
		"message" => "Must be logged in"
	));
	exit;
}

//date below, also need to change the corresponding 's' to whatever format date takes
$stmt->bind_param('sssssssss', $user, $day, $month, $year, $time, $title, $description, $tags, $public);
 
$stmt->execute();
 
$stmt->close();

echo json_encode(array(
		"success" => true,
	));
exit;

}

else{
    echo json_encode(array(
		"success" => false,
		"message" => "Not a user"
	));
	exit;         
}
?>