function searchTag(){
    var t= document.getElementById("tagSearchFrm").value;
    createTagCalendar(t);
    updateCalendarTag(t);
}

function createTagCalendar(tag) {
	var HTML= "<TABLE BORDER=3 CELLSPACING=3 CELLPADDING=50 WIDTH =90%>"
	+ "<TR>"
	+ "<TD COLSPAN=\"7\" ALIGN=center>"
	+ "<h1 id=\"month\">June</h1>"
	+ "<h4 id=\"year\">2015</h4>"
	+ "<input id=\"prevMonth\" type=\"button\" value= \"<----\" onClick=\"prevTagMonth()\">"
	+ "<input id=\"clearBtn\" type=\"button\" value= \"Clear Search\" onClick=\"createCalendar(); updateCalendar()\">"
	+ "<input id=\"nextMonth\" type=\"button\" value= \"---->\" onClick=\"nextTagMonth()\">"
	+ "</TD>"
	+ "</TR>"
	+ "<TR>"
	+ "<TD ALIGN=center>Sunday</TD>"
	+ "<TD ALIGN=center>Monday</TD>"
	+ "<TD ALIGN=center>Tuesday</TD>"
	+ "<TD ALIGN=center>Wednesday</TD>"
	+ "<TD ALIGN=center>Thursday</TD>"
	+ "<TD ALIGN=center>Friday</TD>"
	+ "<TD ALIGN=center>Saturday</TD>"
	+ "</TR>";
	
	
	for(row=1; row<=6; row++) {
	HTML += "<TR>";
		
		for (col=1; col <=7; col++) {
			HTML += "<TD align= left id=" +  (((row-1) *7) + col) + ">" + "</TD>";
		}
		
	HTML += "</TR>";
	}
	HTML+= "</TABLE>";
	var x = document.getElementById("Calendar");
	
	x.innerHTML = HTML;
	
}


function updateCalendarTag(tag){
    var weeks = currentMonth.getWeeks();
	
	document.getElementById("month").innerHTML = monthNames[currentMonth.month];
	document.getElementById("year").innerHTML = currentMonth.year;
	
	var index = 1;
	for(var w in weeks){
		var days = weeks[w].getDates();
		// days contains normal JavaScript Date objects.
 
 
		for(d=0; d<7; d++){
            var moo= currentMonth.month + 1;
            var yoo= currentMonth.year;
            var doo= days[d].getDate();
			// You can see console.log() output in your JavaScript debugging tool, like Firebug,
			// WebWit Inspector, or Dragonfly.
			//console.log(days[d].toISOString());
			var cell = document.getElementById(index);

			if (days[d].getMonth() == currentMonth.month) {
				
				cell.innerHTML = days[d].getDate();
				            
				getEventByTag(doo, moo, yoo, index, tag);
		
			}
			
			else{
				
				cell.innerHTML = "";
				
			}
					
            index++;
		}
	}
}

function getEventByTag(day, month, year, index, tag) {

    // Make a URL-encoded string for passing POST data:
	var dataString = "day=" + encodeURIComponent(day) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year)
    + "&index=" + encodeURIComponent(index) + "&tag=" + encodeURIComponent(tag);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CalendarGetEventByTag.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData	
       
            addTagEvent(day, month, year);
		
        }else{
            
		}
	}, false); // Bind the callback to the load event
    
	xmlHttp.send(dataString); // Send the data
}

function addTagEvent(day, month, year){
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    var cell = document.getElementById(jsonData.index);
	cell.innerHTML+= "<br>";
	cell.innerHTML += "<input type=button value=\"" + jsonData.title + "\" onClick= \"editEvent("
	+ day + "," + month + "," + year + ", \'" + jsonData.title + "\')\">";
	cell.innerHTML += "<br>"+ "<label> Time: " + jsonData.time + "</label>";
	cell.innerHTML += "<br>"+ "<label> Description: " + jsonData.description + "</label>";
}