<?php
//CalendarDeleteEvent.php
//Takes in data, user and title then deletes event from mysql

header("Content-Type: application/json"); 
ini_set("session.cookie_httponly", 1);
session_start();

//connect
$mysqli = new mysqli('localhost', 'Calendar', 'Calendar', 'Calendar');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Can't connect"
	));
	exit;
}

//check if logged in
if(empty($_SESSION["username"])){
	echo json_encode(array(
		"success" => false,
		"message" => "Not logged in"
	));
	exit;
}

//get post data
$title = htmlentities($_POST['title']);
$user = htmlentities($_SESSION['username']);
$day = htmlentities($_POST['day']);
$month = htmlentities($_POST['month']);
$year = htmlentities($_POST['year']);

//Using d/m/y and session username find proper event to delete
$stmt = $mysqli->prepare("delete from events where title= ? and user= ? and day= ? and month= ? and year= ?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}

$stmt->bind_param('sssss', $title, $user, $day, $month, $year);
 
$stmt->execute();
 
$stmt->close();

echo json_encode(array(
		"success" => true,
	));
exit;

?>