<?php
//CalendarCreateEvent
//Takes in event data via post and adds to mysql

header("Content-Type: application/json");

ini_set("session.cookie_httponly", 1);
session_start();

//connect to database
$mysqli = new mysqli('localhost', 'Calendar', 'Calendar', 'Calendar');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Can't connect"
	));
	exit;
}

//if not logged in, don't create event
if(empty($_SESSION["username"])){
	echo json_encode(array(
		"success" => false,
		"message" => "Not logged in"
	));
	exit;
}

//get post data
//use session username as user
$user= htmlentities($_SESSION['username']);
$day = htmlentities($_POST['day']);
$month = htmlentities($_POST['month']);
$year = htmlentities($_POST['year']);
$title= htmlentities($_POST['title']);
$time = htmlentities($_POST['time']);
$description= htmlentities($_POST['description']);
$tags = htmlentities($_POST['tags']);
$public = htmlentities($_POST['public']);

//add into events table
$stmt = $mysqli->prepare("insert into events (user, day, month, year, time, title, description, tags, public) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
if(!$stmt){
    echo json_encode(array(
		"success" => false,
		"message" => "Must be logged in"
	));
	exit;
}


$stmt->bind_param('sssssssss', $user, $day, $month, $year, $time, $title, $description, $tags, $public);
 
$stmt->execute();
 
$stmt->close();

echo json_encode(array(
		"success" => true,
	));
exit;
                  
?>