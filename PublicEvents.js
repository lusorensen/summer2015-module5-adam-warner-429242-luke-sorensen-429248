//same as manage events, uses public events instead of user events

var monthNames = [ "January", "February", "March", "April", "May", "June",
"July", "August", "September", "October", "November", "December" ];

function createPublicCalendar(events) {
	var HTML= "<TABLE BORDER=3 CELLSPACING=3 CELLPADDING=50 WIDTH =90%>"
	+ "<TR>"
	+ "<TD COLSPAN=\"7\" ALIGN=center>"
	+ "<h1 id=\"month\">June</h1>"
	+ "<h4 id=\"year\">2015</h4>"
	+ "<input id=\"prevMonth\" type=\"button\" value= \"<----\" onClick=\"prevPublicMonth()\">"
	+ "<B></B>"
	+ "<input id=\"nextMonth\" type=\"button\" value= \"---->\" onClick=\"nextPublicMonth()\">"
	+ "</TD>"
	+ "</TR>"
	+ "<TR>"
	+ "<TD ALIGN=center>Sunday</TD>"
	+ "<TD ALIGN=center>Monday</TD>"
	+ "<TD ALIGN=center>Tuesday</TD>"
	+ "<TD ALIGN=center>Wednesday</TD>"
	+ "<TD ALIGN=center>Thursday</TD>"
	+ "<TD ALIGN=center>Friday</TD>"
	+ "<TD ALIGN=center>Saturday</TD>"
	+ "</TR>";
	
	
	for(row=1; row<=6; row++) {
	HTML += "<TR>";
		
		for (col=1; col <=7; col++) {
			HTML += "<TD align= left id=" +  (((row-1) *7) + col) + ">" + "</TD>";
		}
		
	HTML += "</TR>";
	}
	HTML+= "</TABLE>";
	var x = document.getElementById("Calendar");
	
	x.innerHTML = HTML;
	
}

function updateCalendarPublic(){
    var weeks = currentMonth.getWeeks();
	
	document.getElementById("month").innerHTML = monthNames[currentMonth.month];
	document.getElementById("year").innerHTML = currentMonth.year;
	
	var index = 1;
	for(var w in weeks){
		var days = weeks[w].getDates();
		// days contains normal JavaScript Date objects.
 
 
		for(d=0; d<7; d++){
            var moo= currentMonth.month + 1;
            var yoo= currentMonth.year;
            var doo= days[d].getDate();
			// You can see console.log() output in your JavaScript debugging tool, like Firebug,
			// WebWit Inspector, or Dragonfly.
			//console.log(days[d].toISOString());
			var cell = document.getElementById(index);

			if (days[d].getMonth() == currentMonth.month) {
				
				cell.innerHTML = days[d].getDate();
				            
				getEventByPublic(doo, moo, yoo, index);
		
			}
			
			else{
				
				cell.innerHTML = "";
				
			}
					
            index++;
		}
	}
}

function getEventByPublic(day, month, year, index) {

    // Make a URL-encoded string for passing POST data:
	var dataString = "day=" + encodeURIComponent(day) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year)
    + "&index=" + encodeURIComponent(index);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "CalendarGetPublic.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData	
       
            addPublicEvent(day, month, year);
		
        }else{
            
		}
	}, false); // Bind the callback to the load event
    
	xmlHttp.send(dataString); // Send the data
}

function addPublicEvent(day, month, year){
    var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
    var cell = document.getElementById(jsonData.index);
	cell.innerHTML+= "<br>";
	cell.innerHTML += "<b>" + jsonData.title + "<b>";
	cell.innerHTML += "<br>"+ "<label> Poster: " + jsonData.user + "</label>";
	cell.innerHTML += "<br>"+ "<label> Description: " + jsonData.description + "</label>";
}