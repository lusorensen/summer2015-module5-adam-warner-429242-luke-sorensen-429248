<?php
//CalendarRegister.php
//takes in username and password and adds to user table

header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);
session_start();

if(empty($_POST['username'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit; 
}

if(empty($_POST['password'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit;
}

$mysqli = new mysqli('localhost', 'Calendar', 'Calendar', 'Calendar');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit; 
}

$username = htmlentities($_POST['username']);
$password = crypt(htmlentities($_POST['password']));

$stmt1 = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");

// Bind the parameter
$stmt1->bind_param('s', $username);
$stmt1->execute();

$result = $stmt1->get_result();

while($row = $result->fetch_assoc()){
	$count= htmlentities($row["COUNT(*)"]);	
}

if($count==0){
$stmt = $mysqli->prepare("insert into users (username, password) values (?, ?)");
if(!$stmt){
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit;
}

$stmt = $mysqli->prepare("insert into users (username, password) values (?, ?)");
if(!$stmt){
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit;
}
 
$stmt->bind_param('ss', $username, $password);
 
$stmt->execute();
 
$stmt->close();


$_SESSION['username'] = $username;
$_SESSION['token'] = substr(md5(rand()), 0, 10);
 
echo json_encode(array(
"success" => true,
"user" => $username
	));
exit;
}

else{
	
	echo json_encode(array(
		"success" => false,
		"message" => "Already a registered user"
	));
	exit; 
}
?>